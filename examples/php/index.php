<?php
require(__DIR__ . '/vendor/autoload.php');
$emails = ['foo@example.org', 'bar@example.org', 'fizbuz@example.net'];
$ob = new \Smorken\Obfuscate\Obfuscate();
foreach ($emails as $email): ?>
    <a class="clr" data-smob="<?php echo $ob->obfuscate($email); ?>" data-smob-overwrite data-smob-prepend="mailto:"></a>
<?php endforeach; ?>
<script src="./../../dist/smob.var.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function () {
    var smob = new SmOb.Obfuscate();
    var smobdom = new SmOb.Dom(smob);
    smobdom.handle(document.querySelectorAll('.clr'), 'clear');
});
</script>