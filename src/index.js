import Obfuscate from './obfuscate'
import Dom from './dom'

export { Obfuscate, Dom }