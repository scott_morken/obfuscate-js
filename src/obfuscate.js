export default class Obfuscate {
  rotation = 6
  split_at = '/'

  constructor (config) {
    if (typeof config !== 'undefined') {
      for (let [key, value] of Object.entries(config)) {
        this._set(key, value)
      }
    }
  }

  obfuscate (str, rotation, split_at) {
    rotation = this._default(rotation, this.rotation)
    split_at = this._default(split_at, this.split_at)
    let split = str.split('')
    return split.map(function (s) {
      return (parseInt(s.charCodeAt(0)) + parseInt(rotation) + split.length)
    }).join(split_at)
  }

  clear (str, rotation, split_at) {
    rotation = this._default(rotation, this.rotation)
    split_at = this._default(split_at, this.split_at)
    let split = str.split(split_at)
    return split.map(function (s) {
      return String.fromCharCode(parseInt(s) - parseInt(rotation) - split.length)
    }).join('')
  }

  _set (k, v) {
    this[k] = v
  }

  _default(v, def) {
    if (typeof v === 'undefined') {
      v = def
    }
    return v
  }
}