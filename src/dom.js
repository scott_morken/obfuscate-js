import Obfuscate from './obfuscate'

export default class Dom {
  all_keys = ['attribute', 'overwrite', 'prepend', 'append']
  data_key = 'smob'
  attribute = 'href'

  constructor (smob, config) {
    if (typeof smob === 'undefined') {
      smob = new Obfuscate()
    }
    this._set('smob', smob)
    if (typeof config !== 'undefined') {
      for (let [key, value] of Object.entries(config)) {
        this._set(key, value)
      }
    }
  }

  handle (elements, action) {
    if (!elements || !elements.length) {
      return
    }
    if (typeof action === 'undefined') {
      action = 'clear'
    }
    for (let i = 0; i < elements.length; i++) {
      let element = elements[i]
      this.modifyElement(element, action)
      this.clearDataAttributes(element)
    }
  }

  modifyElement (element, action) {
    let raw = this._getFromElement(element, this._getDataSetKey())
    if (raw !== false) {
      let modified = this._action(raw, action)
      let attribute = this._getFromElement(element, this._getDataSetKey('attribute'), this.attribute)
      let overwrite = this._getFromElement(element, this._getDataSetKey('overwrite'))
      if (attribute) {
        let prepend = this._getFromElement(element, this._getDataSetKey('prepend'), '')
        let append = this._getFromElement(element, this._getDataSetKey('append'), '')
        element[attribute] = prepend + modified + append
      }
      if (overwrite !== false) {
        element.innerHTML = modified
      }
    }
  }

  clearDataAttributes (element) {
    this._removeAttribute(element, this._getDataKey())
    for (let i = 0; i < this.all_keys.length; i++) {
      this._removeAttribute(element, this._getDataKey(this.all_keys[i]))
    }
  }

  _action (raw, action) {
    if (typeof action === 'function') {
      return action(raw)
    }
    switch (action.charAt(0).toLowerCase()) {
      case 'c':
        return this.smob.clear(raw)
      case 'o':
        return this.smob.obfuscate(raw)
    }
  }

  _removeAttribute (element, attribute) {
    element.removeAttribute(attribute)
  }

  _getDataKey (type, as_array) {
    if (typeof as_array === 'undefined') {
      as_array = false
    }
    let key = ['data', this.data_key]
    if (typeof type !== 'undefined') {
      let exp = type.split('-')
      for (let i = 0; i < exp.length; i++) {
        let val = exp[i]
        if (val) {
          key.push(exp[i])
        }
      }
    }
    return as_array ? key : key.join('-')
  }

  _getDataSetKey (type) {
    let keys = this._getDataKey(type, true)
    let new_keys = []
    for (let i = 1; i < keys.length; i++) {
      let val = keys[i]
      if (val) {
        if (i > 1) {
          val = val.charAt(0).toUpperCase() + val.slice(1)
        }
        new_keys.push(val)
      }
    }
    return new_keys.join('')
  }

  _getFromElement (element, data_key, def) {
    if (typeof def === 'undefined') {
      def = false
    }
    let item = element.dataset[data_key]
    if (typeof item === 'undefined') {
      return def
    }
    return item
  }

  _set (k, v) {
    this[k] = v
  }
}