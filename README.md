### Javascript Obfuscate library

This library is an obfuscation helper. Note that it is _obfuscation_
and **not** encryption.  It is incredibly easy to "figure out" the
clear text value.  Its sole purpose is to make it more difficult
for bots and web scrapers (most of which don't do javascript) to scrape
things like email addresses and phone numbers from crawlable pages.

#### Installation

* npm install smorken-obfuscate
* copy (or use an asset manager, etc) `smorken-obfuscate/dist/smob.var.js` to your js assets
directory
* `smob.commonjs2.js` and `smob.umd.js` are also available if that's how
you're rolling

#### Use

* data attributes
  * data-smob: contains the obfuscated value
  * data-smob-overwrite: overwrite the elements inner html with the clear text value
  * data-smob-append: clear text to append to the attribute text (not inner html)
  * data-smob-prepend: clear text to prepend to the attribute text (not inner html)
  * data-smob-attribute: the attribute to attach the clear text value (defaults to href)

There are two 'classes' in the SmOb object (exposed by smob.var.js).

`SmOb.Obfuscate`:
* `clear(str, [int rotation, [char split_at]])`: clears obfuscation
* `obfuscate(str, [int rotation, [char split_at]])`: obfuscates string

`SmOb.Dom`:
* `constructor([SmOb.Obfuscate, [array config]])`
* `handle(array elements, string|callable action)`: iterates elements and applies action ('clear', 'obfuscate')
to each and updates the DOM
* `modifyElement(element, string|callable action)`: applies the action to the element
and updates the DOM
* `clearDataAttributes(element)`: removes the smob data attributes from the element

##### PHP library

`smorken/obfuscate` library works with this library
```
$ composer install smorken/obfuscate
```

##### Examples
Clear obfuscated values (on page load)
```html
<a class="some-class" data-smob="138/123/137/86" data-smob-overwrite data-smob-prepend="mailto:"></a>
<script src="assets/smob.var.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function () {
    var smob = new SmOb.Obfuscate();
    var smobdom = new SmOb.Dom(smob);
    smobdom.handle(document.querySelectorAll('.some-class'), 'clear')
});
</script>
``` 
Clear obfuscated value (on click)
```html
<a class="some-class" data-smob="138/123/137/86" data-smob-overwrite data-smob-prepend="mailto:">Click to show</a>
<script src="assets/smob.var.js"></script>
<script>
document.addEventListener('click', function (event) {
    if (!event.target.matches('.some-class')) return
    event.preventDefault()
    let smob = new SmOb.Obfuscate()
    let smobdom = new SmOb.Dom(smob)
    smobdom.modifyElement(event.target, 'clear')
  })
</script>
```
