const expect = require('chai').expect
import { Obfuscate, Dom } from './../src/index'
const jsdom = require('jsdom')
const { JSDOM } = jsdom

describe('SmOb Dom', function () {
  it('should obfuscate an anchor tag', function () {
    let smob = new Obfuscate()
    let sut = new Dom(smob)
    let html = '<html><head></head><body><a class="ob" data-smob="test@example.org" data-smob-prepend="mailto:" data-smob-overwrite></a></body></html>'
    let dom = new JSDOM(html).window
    sut.handle(dom.document.querySelectorAll('.ob'), 'obfuscate')
    let element = dom.document.querySelector('a')
    expect(element.innerHTML).to.equal('138/123/137/138/86/123/142/119/131/134/130/123/68/133/136/125')
    expect(element.getAttribute('href')).to.equal('mailto:138/123/137/138/86/123/142/119/131/134/130/123/68/133/136/125')
    expect(element.dataset).to.not.have.property('data-smob')
  })
  it('should clear an anchor tag', function () {
    let smob = new Obfuscate()
    let sut = new Dom(smob)
    let html = '<html><head></head><body><a class="clr" data-smob="138/123/137/138/86/123/142/119/131/134/130/123/68/133/136/125" data-smob-prepend="mailto:" data-smob-overwrite></a></body></html>'
    let dom = new JSDOM(html).window
    sut.handle(dom.document.querySelectorAll('.clr'))
    let element = dom.document.querySelector('a')
    expect(element.innerHTML).to.equal('test@example.org')
    expect(element.getAttribute('href')).to.equal('mailto:test@example.org')
    expect(element.dataset).to.not.have.property('data-smob')
  })
  it('should obfuscate a div tag', function () {
    let smob = new Obfuscate()
    let sut = new Dom(smob)
    let html = '<html><head></head><body><div class="ob" data-smob="foo bar biz buz" data-smob-overwrite></div></body></html>'
    let dom = new JSDOM(html).window
    sut.handle(dom.document.querySelectorAll('.ob'), 'obfuscate')
    let element = dom.document.querySelector('div')
    expect(element.innerHTML).to.equal('123/132/132/53/119/118/135/53/119/126/143/53/119/138/143')
    expect(element.getAttribute('href')).to.be.a('null')
    expect(element.dataset).to.not.have.property('data-smob')
  })
  it('should clear a div tag', function () {
    let smob = new Obfuscate()
    let sut = new Dom(smob)
    let html = '<html><head></head><body><div class="clr" data-smob="123/132/132/53/119/118/135/53/119/126/143/53/119/138/143" data-smob-overwrite></div></body></html>'
    let dom = new JSDOM(html).window
    sut.handle(dom.document.querySelectorAll('.clr'), 'clear')
    let element = dom.document.querySelector('div')
    expect(element.innerHTML).to.equal('foo bar biz buz')
    expect(element.getAttribute('href')).to.be.a('null')
    expect(element.dataset).to.not.have.property('data-smob')
  })
})