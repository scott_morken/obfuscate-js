const expect = require('chai').expect
import { Obfuscate } from './../src/index'

describe('SmOb Obfuscate', function () {
  it('should obfuscate a string', function () {
    let sut = new Obfuscate()
    let o = sut.obfuscate('test string')
    expect(o).to.equal('133/118/132/133/49/132/133/131/122/127/120')
  })
  it('should return clear text', function () {
    let sut = new Obfuscate()
    let o = sut.clear('133/118/132/133/49/132/133/131/122/127/120')
    expect(o).to.equal('test string')
  })
})
