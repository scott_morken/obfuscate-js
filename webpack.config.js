const path = require('path')

function createConfig (target) {
  return {
    mode: (process.env.NODE_ENV ? process.env.NODE_ENV : 'development'),
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'smob.' + target + '.js',
      library: 'SmOb',
      libraryTarget: target
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          }
        },
      ]
    },
    devtool: ('production' === process.env.NODE_ENV ? 'source-map' : 'cheap-module-eval-source-map')
  }
}

module.exports = [
  createConfig('var'),
  createConfig('commonjs2'),
  createConfig('umd')
]